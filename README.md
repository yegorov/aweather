AWeather Library
========

This is open source library for weather forecast subject.

version 1.0

Example, how use **ParseFactory**, **WeatherParsable**, **Weather**.
```Java
    ParseFactory parseFactory = ParseFactory.getParseFactory();
    WeatherParsable weatherParsable = parseFactory.getParserInstance(ForecastProvider.InmartWeather);
	
    Weather currentWeather = null;
	
    String airTemp       = "Unknown",
           barPress      = "Unknown",
           humidity      = "Unknown",
           cloudiness    = "Unknown",
           precipitation = "Unknown",
           windDirection = "Unknown",
           windSpeed     = "Unknown";
    try {
                                                // String get in Internet
        currentWeather = weatherParsable.stringParse("99~734~2.6~1.7~12"); 

        airTemp       = currentWeather.airTempToStr();
        barPress      = currentWeather.barPressToStr();
        humidity      = currentWeather.humidityToStr();
        cloudiness    = currentWeather.cloudinessToStr();
        precipitation = currentWeather.precipitationToStr();
        windDirection = currentWeather.windDirectionToStr();
        windSpeed     = currentWeather.windSpeedToStr();
		
    } catch (NoValidWeatherData noValidWeatherData) {
        noValidWeatherData.printStackTrace();
    }

    System.out.print("Temperature:    " + airTemp       + "\n" +
                     "Pressure:       " + barPress      + "\n" +
                     "Humidity:       " + humidity      + "\n" +
                     "Cloudiness:     " + cloudiness    + "\n" +
                     "Precipitation:  " + precipitation + "\n" +
                     "Wind direction: " + windDirection + "\n" +
                     "Wind speed:     " + windSpeed);

```


Calculate Moon's properties.
```Java
    Moon moon = new Moon();
    String moonAge;
    String moonPhase;
    try {
        // day, month, year
        moon.init(11, 2, 2015);
        moonAge = String.format("%.1f", moon.getMoonAge());
        moonPhase = moon.getMoonPhase().toStr();

    } catch (NoValidDateException e) {
        moonAge = "Unknown";
        moonPhase = "Unknown";
        System.out.println(e.getMessage());
    }
    System.out.println("moon age: " + moonAge + "\nmoon phase: " + moonPhase);
```

Calculate Sun's properties.
```Java
    Sun s = new Sun();
    String sunRiseHHMM;
    String sunSetHHMM;
    try {
        // day, month, year, latitude, longitude, local time zone
        s.init(11, 2, 2015, 48.333611, 38.0925, +2);
        sunRiseHHMM = s.toStrSunRiseHHMM();
        sunSetHHMM = s.toStrSunSetHHMM();
    } catch (NoValidDateException e) {
        sunRiseHHMM = "Unknown";
        sunSetHHMM = "Unknown";
        System.out.println(e.getMessage());
    }
    System.out.println(sunRiseHHMM + "   " + sunSetHHMM);
```

Other snippets
```Java
    Sun sun = new Sun();
    Moon moon = new Moon();
    
    moon.setParameters(new Moon.MoonBuilder(MoonPhase.NewMoon, Zodiac.Aquarius, 10.9).build());

    sun.setParameters(Time.valueOfSeconds(6788), Time.valueOfHour(10.4));

    DataDay dataDay = DataDay.newInstance(sun, moon);

    System.out.println(dataDay.getSun().toStrSunRiseHHMM());

    WindDirection w = WindDirection.values()[WindDirection.E.ordinal()];
    System.out.println(w.toStr());
```
