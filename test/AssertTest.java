package com.yegorov;

import com.yegorov.modelweather.Cloudiness;
import com.yegorov.modelweather.Precipitation;
import com.yegorov.modelweather.Weather;
import com.yegorov.modelweather.WindDirection;
import com.yegorov.parse.ForecastProvider;
import com.yegorov.parse.NoValidWeatherData;
import com.yegorov.parse.ParseFactory;
import com.yegorov.parse.WeatherParsable;
import com.yegorov.units.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests
 */
public class AssertTest {

    double delta = 0.001;

    @Test
    public void testDefaultEquals() {
        Weather w = new Weather.WeatherBuilder().build();

        Assert.assertEquals(w.getTemperatureValue(), 0.0, delta);
        Assert.assertEquals(w.getTemperatureUnit(), TemperatureUnits.C);

        Assert.assertEquals(w.getBarometricPressureValue(), 0.0, delta);
        Assert.assertEquals(w.getBarometricPressureUnit(), PressureUnits.mm_Hg);

        Assert.assertEquals(w.getWindSpeedValue(), 0.0, delta);
        Assert.assertEquals(w.getWindSpeedUnit(), SpeedUnits.m_s);

        Assert.assertEquals(w.getWindDirection(), WindDirection.Indeterminately);

        Assert.assertEquals(w.getHumidity(), 0.0, delta);

        Assert.assertEquals(w.getCloudiness(), Cloudiness.Indeterminately);

        Assert.assertEquals(w.getPrecipitation(), Precipitation.Indeterminately);

    }

    @Test
        public void testInmartWeatherEquals() {
        ParseFactory parseFactory = ParseFactory.getParseFactory();
        WeatherParsable weatherParsable = parseFactory.getParserInstance(ForecastProvider.InmartWeather);
        Weather w = null;
        try {
            w = weatherParsable.stringParse("99~734~2.6~1.7~12");
        } catch (NoValidWeatherData noValidWeatherData) {
            noValidWeatherData.printStackTrace();
        }

        Assert.assertNotNull(w);

        Assert.assertEquals(w.getTemperatureValue(), 2.6, delta);
        Assert.assertEquals(w.getTemperatureUnit(), TemperatureUnits.C);

        Assert.assertEquals(w.getBarometricPressureValue(), 734, delta);
        Assert.assertEquals(w.getBarometricPressureUnit(), PressureUnits.mm_Hg);

        Assert.assertEquals(w.getWindSpeedValue(), 1.7, delta);
        Assert.assertEquals(w.getWindSpeedUnit(), SpeedUnits.m_s);

        Assert.assertEquals(w.getWindDirection(), WindDirection.E);

        Assert.assertEquals(w.getHumidity(), 99, delta);

        Assert.assertEquals(w.getCloudiness(), Cloudiness.Indeterminately);

        Assert.assertEquals(w.getPrecipitation(), Precipitation.Indeterminately);

    }

}
